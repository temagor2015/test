<?php

namespace Database\Seeders;

use App\Models\Resource\Article;
use App\Models\Resource\Tag;
use Illuminate\Database\Seeder;

class ArticleTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = Tag::all();
        foreach (Article::all() as $article) {
            $article->tags()->attach($tags->random(rand(1, 6)));
        }
    }
}
