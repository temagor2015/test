<?php

namespace App\Http\Controllers\Resource\Api;

use App\Models\Resource\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Resource\Tag;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Article::with('tags')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:160',
            'content' => 'required|max:65535',
            'tags' => 'array',
            'tags.name' => 'string|distinct|exists:App\Models\Resource\Tag,name'
        ]);

        $tags = collect($validated['tags']);
        $article = Article::create(['name' => $validated['name'], 'content' => $validated['content']]);
        $article->tags()->attach(Tag::whereIn('name', $tags->pluck('name')->all())->get());

        return response()->json($article->load('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resource\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return response()->json($article->load('tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resource\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $validated = $request->validate([
            'name' => 'required|max:160',
            'slug' => 'required|exists:App\Models\Resource\Article,slug',
            'content' => 'required|max:65535',
            'tags' => 'array',
            'tags.name' => 'string|distinct|exists:App\Models\Resource\Tag,name'
        ]);

        $article = Article::whereSlug($validated['slug'])->firstOrFail();
        $article->update(['name' => $validated['name'], 'content' => $validated['content']]);

        $tags = collect($validated['tags']);
        if ($tags->isNotEmpty()) {
            $article->tags()->sync(Tag::whereIn('name', $tags->pluck('name')->all())->get());
        }

        return response()->json($article->load('tags'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resource\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->tags()->sync([]);
        $article->delete();
        return response()->json();
    }
}
